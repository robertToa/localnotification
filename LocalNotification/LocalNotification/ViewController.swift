//
//  ViewController.swift
//  LocalNotification
//
//  Created by Robert on 10/1/18.
//  Copyright © 2018 Robert. All rights reserved.
//

import UIKit
import UserNotifications

class ViewController: UIViewController, UNUserNotificationCenterDelegate {

    //:- Variables
    var notificationMessage = "El texto"
    
    //:-Outlet
    @IBOutlet weak var outputLabelText: UILabel!
    @IBOutlet weak var inputLabelText: UITextField!
    
    //:-Load y principal
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //Para recuperar
        //1.- Instanciar el userdefaults
        let defaults = UserDefaults.standard
        
        //2.- Acceder al valor por medio del Key
        let text = defaults.object(forKey: "label") as? String
        outputLabelText.text = text
        
        UNUserNotificationCenter.current().delegate = self
        
        UNUserNotificationCenter.current().requestAuthorization(options: [.badge, .sound, .alert]){ (granted, error) in
        }
    }

    
    
    //:-Action
    @IBAction func guardarInputButton(_ sender: Any) {
        outputLabelText.text = inputLabelText.text
        
        //i nstanciar USERDEFAULTS
        let defaults = UserDefaults.standard
        
        //guardar variables en defaulta que pueden ser int, double, object ,etc
        defaults.set(inputLabelText.text, forKey: "label")
        
        //LLamar a la fucnion sendNotification
        sendNotification()
        
    }
    
    func sendNotification(){
        notificationMessage += outputLabelText.text!
        
        
        
        //Agregar acciones y categorias a las notificaciones
        let replyAction = UNTextInputNotificationAction(identifier: "change", title: "Reply", options: [], textInputButtonTitle: "Send", textInputPlaceholder: "Type your message")
        let deleteAction = UNNotificationAction(identifier: "delete", title: "Delete", options: [.authenticationRequired, .destructive])
        let category = UNNotificationCategory(identifier: "category", actions: [deleteAction,replyAction], intentIdentifiers: [], options: [])
        
        UNUserNotificationCenter.current().setNotificationCategories([category])
        
        //1.- Authorization request (esta en Didload)
        //2.- Crear contenido de la notification
        
        let content = UNMutableNotificationContent()
        content.title = "Notification Title"
        content.subtitle = "Notification Subtitle"
        content.body = notificationMessage
        content.categoryIdentifier = "category"
        //3.-Definir un trigger
        
        let trigger = UNTimeIntervalNotificationTrigger(timeInterval: 5, repeats: false)
        
        //4.- Definir un identificador para la notification
        let identifier = "Notification"
        
        //5.- Crear un request
        let notificationRequest = UNNotificationRequest(identifier: identifier, content: content, trigger:trigger)
        
        // Añadir el request al UNUserNotificationCenter
        UNUserNotificationCenter.current().add(notificationRequest){ (error) in
        }
        
    }
    
    
    
    func userNotificationCenter(_ center: UNUserNotificationCenter, didReceive response: UNNotificationResponse, withCompletionHandler completionHandler: @escaping () -> Void) {
        if response.actionIdentifier == "change" {
            if let textResponse = response as? UNTextInputNotificationResponse {
                let reply = textResponse.userText
                
                self.outputLabelText.text = reply
                
                
                let defaults = UserDefaults.standard
                defaults.set(reply, forKey: "label")
                
                //completionHandler()
            }
        }else if response.actionIdentifier == "delete" {
            UNUserNotificationCenter.current().removeDeliveredNotifications(withIdentifiers: ["Notification"])
        }
    }
    
    
    
    
    
}

